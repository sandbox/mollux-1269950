<?php

/**
 * @file
 * Provides the spatial element to use in forms
 */

/**
 * Implements hook_element_info().
 */
function search_api_spatial_element_info() {
  $elements = array();
  $elements['search_api_spatial_pick'] =  array(
    '#input' => FALSE,
    '#process' => array('search_api_spatial_element_process'),
   // '#theme' => 'locationradius',
    '#theme_wrappers' => array('form_element'),
    '#options' => array(),
  );
  
  return $elements;
}

/**
 * Processor for the search_api_spatial_pick field
 */
function search_api_spatial_element_process($element, $form_state, $complete_form) {

  $id = $element['#id'];
  $form_id = $complete_form['form_id'];
  
  $lat_value = isset($form_state['input']['lat']) ? $form_state['input']['lat'] : $element['#default_value']['lat'];
  $lng_value = isset($form_state['input']['lng']) ? $form_state['input']['lng'] : $element['#default_value']['lng'];
  $radius_value = isset($form_state['input']['radius']) ? $form_state['input']['radius'] : $element['#default_value']['radius'];
     
  $element['address'] = array(
    '#type' => 'textfield',
    '#maxlength' => 120,
    '#attributes' => array(
      'id' => $id . '-address',
    ),
    '#field_suffix' => '<a id="' . $id . '-geocode">' . t('Get location') . '</a>',
  );

  $element['help'] = array(
    '#attributes' => array(
      'id' => $id . '-help',        
    ),
    '#markup' => t('Enter an address / location in the textfield or click on the map to set the marker'),
  );
  
  $element['gmap'] = array(
    '#type' => 'container',
    '#attributes' => array(
      'id' => $id . '-gmap',
      'style' => array('width:100%; height:400px;'),
    ),
  );

  //TODO : make the suffix configurable to support miles
  //TODO : move the radius and slider out of here
  
  $element['radius'] = array(
    '#type' => 'textfield',
    '#title' => t('Radius'),
    '#size' => 3,
    '#default_value' => $radius_value,
    '#field_suffix' => "km",
    '#parents' => array_merge($element['#parents'], array('radius')),
  );
  
  $element['slider'] = array(
    '#type' => 'container',
    '#attributes' => array(
      'id' => $id . '-slider',
    ),
  );

  $element['lat'] = array(
    '#type' => 'hidden',
    '#attributes' => array(
      'id' => $id . '-lat',        
    ),
    '#default_value' => $lat_value,
    '#parents' => array_merge($element['#parents'], array('lat')),
  );

  $element['lng'] = array(
    '#type' => 'hidden',
    '#attributes' => array(
      'id' => $id . '-lng',        
    ),
    '#default_value' => $lng_value,
    '#parents' => array_merge($element['#parents'], array('lng')),
  );
  
  // add the javascript stuff

  drupal_add_library('system', 'ui.slider');
     
  $element['googlemap']['#attached']['js'][] = array(
    'data' => 'http://www.google.com/jsapi', 
    'type' => 'external'
  );
  
  $element['googlemap']['#attached']['js'][] = array(
    'data' => 'http://maps.google.com/maps/api/js?sensor=false', 
    'type' => 'external'
  );
  
  $element['googlemap']['#attached']['js'][] = array(
    'data' => drupal_get_path('module', 'search_api_spatial') . '/search_api_spatial.pick.js',
    'type' => 'file',
    'scope' => 'footer'
  );
  
  //defining lat and long not necessary, because the are already in the hidden fields
  
  $searchapispatial = array(
    $id => array(
      'lat' => ($lat_value) ? $lat_value : '',
      'lng' => ($lng_value) ? $lng_value : '',
    ),
  );
  
  $element['googlemap']['#attached']['js'][] = array('data' => array('searchapispatial' => $searchapispatial), 'type' => 'setting');

  return $element;
}
