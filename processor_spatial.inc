<?php

/**
 * @file
 * Provides the processor for location search
 */
 
class spatialProcessor extends SearchApiAbstractProcessor {


  public function __construct(SearchApiIndex $index, array $options = array()) {
    parent::__construct($index, $options);
    $this->options += array(
      'spatial_searchtype' => 1
    );
  }
  
  public function supportsIndex(SearchApiIndex $index) {
     
    foreach ($index->options['fields'] as $key => $value)
      if ($value['indexed'] && $value['type'] == 'location') 
        return TRUE;
    
    return FALSE;  
  }

  public function configurationForm() {
    $form = parent::configurationForm();
        
    foreach ($form['fields']['#options'] as $key => $value) {
      if ($this->index->options['fields'][$key]['type'] != 'location') {
        unset($form['fields']['#options'][$key]);
        unset($form['fields']['#default_value'][$key]);
      }
    }
   
    $form += array(
      'spatial_searchtype' => array(
        '#type' => 'radios',
        '#title' => t('Search type'),
        '#description' => t('Select the type of spatial search you want to use.'),
        '#options' => array(
          '1' => 'distance around a point (latitude, longitude and radius)',
      //    '2' => 'bounding box (everything in a box from thel left top to the right bottom)',
        ),
        '#default_value' => $this->options['spatial_searchtype'],      
      ),
      
      /*
      'locatie' => array(
        '#type' => 'locationradius',
        '#weight' => -1,
        '#options' => array(
          'lat' => isset($latlngradius['lat']) ? $latlngradius['lat'] : '51.03826129999999',
          'lng' => isset($latlngradius['lng']) ? $latlngradius['lng'] : '3.7125661000000036',
          'radius' => isset($latlngradius['radius']) ? $latlngradius['radius'] : '0.5',
          'field' => $latlngradius['field'],
        ),
      ),
*/      
      
    );
    
    return $form;
  }

}
