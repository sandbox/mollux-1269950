

(function ($) {

  var geocoder;
  Drupal.searchapispatial = new Object();
  Drupal.searchapispatial.maps = new Array();
  Drupal.searchapispatial.markers = new Array();
  Drupal.searchapispatial.circles = new Array();

  geocoder = new google.maps.Geocoder();

  var lat;
  var lng;
  var latLng;
  var myOptions;
  var browserSupportFlag =  new Boolean();
  var singleClick;
  var circle;


/**
   * Set the latitude and longitude values to the input fields
   * And optionaly update the address field
   *
   * @param latLng
   *   a location (latLng) object from google maps api
   * @param i
   *   the index from the maps array we are working on
   * @param op
   *   the op that was performed
   */
  Drupal.searchapispatial.codeLatLng = function(latLng, i, op) {

    // Update the lat and lng input fields
    $('#'  + i + '-lat').val(latLng.lat());
    $('#'  + i + '-lng').val(latLng.lng());
 
    // Update the address field
    if ((op == 'marker' || op == 'geocoder') && geocoder) {

      geocoder.geocode({ 'latLng' : latLng }, function(results, status) {

        if (status == google.maps.GeocoderStatus.OK) {
          $("#" + i + "-address").val(results[0].formatted_address);        
        }
        else {
          $("#" + i + "-address").val('');
          if (status != google.maps.GeocoderStatus.ZERO_RESULTS) {
            alert(Drupal.t('Geocoder failed due to: ') + status);
          }
        }
      });
    }
  }
 
  /**
   * Get the location from the address field
   *
   * @param i
   *   the index from the maps array we are working on
   */
  Drupal.searchapispatial.codeAddress = function(i) {
    var address = $("#" + i + "-address").val();
    
    geocoder.geocode( { 'address': address }, function(results, status) {
      if (status == google.maps.GeocoderStatus.OK) {
        Drupal.searchapispatial.maps[i].setCenter(results[0].geometry.location);
        Drupal.searchapispatial.setMapMarker(results[0].geometry.location, i);
        Drupal.searchapispatial.codeLatLng(results[0].geometry.location, i, 'textinput');      
      } else {
        alert(Drupal.t('Geocode was not successful for the following reason: ') + status);
      }
    });
  }
   
  /**
   * Set/Update a marker on a map
   *
   * @param latLng
   *   a location (latLng) object from google maps api
   * @param i
   *   the index from the maps array we are working on
   */
  Drupal.searchapispatial.setMapMarker = function(latLng, i) {
    // remove old marker and circle    
    if (Drupal.searchapispatial.markers[i]) {
      Drupal.searchapispatial.markers[i].setMap(null);
      Drupal.searchapispatial.circles[i].setMap(null);
    }
    
    // add marker
    Drupal.searchapispatial.markers[i] = new google.maps.Marker({
      map: Drupal.searchapispatial.maps[i],
      draggable: true,
      animation: google.maps.Animation.DROP,
      position: latLng
    });

    // add circle
    //TODO : allow other distances than kilometer
    //TODO : allow custom colors
    Drupal.searchapispatial.circles[i] = new google.maps.Circle({
      map: Drupal.searchapispatial.maps[i],
      clickable:false, 
      strokeColor:'#ffcc00', 
      fillColor:'#cc3300', 
      radius: $("#" + i + "-slider").slider( "value" ) * 1000, 
      center: latLng
    });

    // fit the map to te circle
    Drupal.searchapispatial.maps[i].fitBounds(Drupal.searchapispatial.circles[i].getBounds()); 
    
    return false; // if called from <a>-Tag
  }

  // Work on each map
  $.each(Drupal.settings.searchapispatial, function(i, searchapispatial) {

      
      $("#"+ i +'-gmap').once('process', function(){
        
        lat = parseFloat(searchapispatial.lat);
        lng = parseFloat(searchapispatial.lng);
                     
        latLng = new google.maps.LatLng(lat, lng);

        // Set map options
        myOptions = {
          zoom: 2,
          center: latLng,
          mapTypeId: google.maps.MapTypeId.ROADMAP,
          scrollwheel: false
        }

        // Create map
        Drupal.searchapispatial.maps[i] = new google.maps.Map(document.getElementById(i + "-gmap"), myOptions);

        // create slider
    		$("#" + i + "-slider").slider({
	        value:$("#" + i + "-radius").val(),
	        min: 0,
	        max: 500,
	        step: 10,
	        slide: function( event, ui ) {
		        $("#"+ i +"-radius").val( ui.value );
		        Drupal.searchapispatial.setMapMarker(Drupal.searchapispatial.markers[i].getPosition(), i);
	        }
        });

	      $("#" + i + "-radius").val($("#" + i + "-slider").slider( "value" ) );


        if (lat && lng) {
          // Set initial marker
          Drupal.searchapispatial.setMapMarker(latLng, i);
          Drupal.searchapispatial.codeLatLng(latLng, i, 'geocoder');
        }
        
        $("#" + i + "-geocode").click(function(e) {
          Drupal.searchapispatial.codeAddress(i);
        });          
        
        // trigger on enter key
        $("#" + i + "-address").keypress(function(ev){
          if(ev.which == 13){
            ev.preventDefault();
            Drupal.searchapispatial.codeAddress(i);
          }
        });
        
        // Listener to click
        google.maps.event.addListener(Drupal.searchapispatial.maps[i], 'click', function(me){
          // Set a timeOut so that it doesn't execute if dbclick is detected
          singleClick = setTimeout(function(){
            Drupal.searchapispatial.codeLatLng(me.latLng, i, 'marker');
            Drupal.searchapispatial.setMapMarker(me.latLng, i);
          }, 500);
        });

        // Detect double click to avoid setting marker
        google.maps.event.addListener(Drupal.searchapispatial.maps[i], 'dblclick', function(me){
          clearTimeout(singleClick);
        });
        
        // Listener to dragend
        google.maps.event.addListener(Drupal.searchapispatial.markers[i], 'dragend', function(me){

            Drupal.searchapispatial.codeLatLng(me.latLng, i, 'marker');
            Drupal.searchapispatial.setMapMarker(me.latLng, i);
        });
        
      })  
    });
}
)(jQuery);

