<?php

require_once('search_api_spatial.elements.inc');

function search_api_spatial_search_api_processor_info() {

  $callbacks['spatial'] = array(
    'name' => t('Spatial search'),
    'description' => t('Processor for spatial search on location fieds.'),
    'class' => 'spatialProcessor',
  );

  return $callbacks;
}

function search_api_spatial_views_data_alter(&$data) {

  foreach (search_api_index_load_multiple(FALSE) as $index) {

    $sf = _search_api_spatial_get_spatialfield($index->id);

    if ($sf) {

      $spatialfield = &$data['search_api_index_' . $index->machine_name][$sf];

      $spatialfield['filter']['handler'] = 'SearchApiViewsHandlerFilterLocation';
      $spatialfield['filter']['type'] = 'location';

      /*
      $spatialfield = &$data['search_api_index_' . $index->machine_name]["source_" . $sf];
      $spatialfield['type'] = 'location';  
      */
    }
  }  
}

function search_api_spatial_form_alter(&$form, &$form_state, $form_id) {
  
  if ($form_id == "search_api_page_search_form") {

    $page_id = $form['form']['id']['#value'];
    $page = search_api_page_load($page_id);

    $spatialfield =  _search_api_spatial_get_spatialfield($page->index_id);

    if ($spatialfield) {
      $spatial = _search_api_spatial_get_spatial($form['form']['keys_' . $page_id]['#default_value']);

      $form['form']['spatial_' . $page_id] = array(
        '#type' => 'search_api_spatial_pick',
        '#weight' => -1,
        '#default_value' => array(
          'lat' => isset($spatial['lat']) ? $spatial['lat'] : '51.03826129999999',
          'lng' => isset($spatial['lng']) ? $spatial['lng'] : '3.7125661000000036',
          'radius' => isset($spatial['radius']) ? $spatial['radius'] : '5',
        ),
        '#tree' => TRUE,
      );

      //TODO : move radius out of geofield en put it here, where it belongs

      $form['form']['keys_' . $page_id]['#default_value'] = _search_api_spatial_get_keys($form['form']['keys_' . $page_id]['#default_value']);

      $form['#submit'][] = "search_api_spatial_search_api_page_search_form_submit";
      $form['#validate'] = array("search_api_spatial_search_api_page_search_form_validate");
    }
  }

  /*
  if ($form_id == "views_ui_edit_form") {
  
    $spatialfield = _search_api_spatial_get_spatialfield($form_state['build_info']['args'][0]->query->getIndex()->id);
    if ($spatialfield) {
     //   drupal_add_js('http://www.google.com/jsapi', 'external');;
       // drupal_add_js('http://maps.google.com/maps/api/js?sensor=FALSE', 'external');
    }
  }
  */
}

function search_api_spatial_search_api_page_search_form_validate(array $form, array &$form_state) {
  //TODO : validate the lat, lng and radius value
}

/**
 * This submit function is only added to the form submit list if the index supports spatial search
 * Checking if the index has a spatial field is not necessary
 */

function search_api_spatial_search_api_page_search_form_submit(array $form, array &$form_state) {

  $page_id = $form_state['values']['id'];
  $spatial = $form_state['values']['spatial_' . $page_id];

  // append the spatial data afther the keys and change the redirect
  // maybe we should add token support and work with slashes

  $form_state['values']['keys_' . $page_id] .= "--spatial--" . $spatial['lat'] . "_" . $spatial['lng'] . "_" . $spatial['radius'];
  $form_state['redirect'] .= "--spatial--" . $spatial['lat'] . "_" . $spatial['lng'] . "_" . $spatial['radius'];

  /*
  Old way of appending to the search redirect.
  
  $form_state['redirect'] = $form_state['values']['base_' . $page_id] . '/' .  "spatial";

  $form_state['redirect'] = array(
    $form_state['redirect'],
    array(
      'query' => array(
        "spatial[lat]" => $form_state['values']['lat'],
        "spatial[lng]" => $form_state['values']['lng'],
        "spatial[radius]" => $form_state['values']['radius'], 
      ),
    ),
  );
  */ 
}

function search_api_spatial_search_api_query_alter(SearchApiQueryInterface $query) {

  $spatialfield = _search_api_spatial_get_spatialfield($query->getIndex()->id);
  $search_id = explode(':', $query->getOption('search id'));

  if ($spatialfield && $search_id[0] == "search_api_page") {

    $keys = $query->getKeys();
    $spatial = _search_api_spatial_get_spatial($keys);

    $spatial['field'] = $spatialfield;

    $query->setOption('spatial', $spatial);

    $query->keys(_search_api_spatial_get_keys($keys));
    $keys = $query->getKeys();
  }
  elseif ($spatialfield && $search_id[0] == "search_api_views") {
    
    // everyting happens in the location filter
  
  }
}


function search_api_spatial_search_api_solr_query_alter(array &$call_args, SearchApiQueryInterface $query) {

  $spatial = $query->getOption('spatial');
    
  if (is_array($spatial)) {
    $call_args['params']['fq'][] = "{!geofilt sfield=locs_" . $spatial['field'] . " pt=" . $spatial['lat'] . ',' . $spatial['lng'] . " d=" . $spatial['radius'] . "}";
    
    /*
    This doesn't work because the filter value isn't evaluated after this hook is called.
    Only adding the fq as a parameter (see above) works.
    
    $filter = $query->createfilter();
    $filter->condition('fq','{!geofilt}' );
    $filter->condition('sfield','locs_' . $spatial['field']);
    $filter->condition('pt', $spatial['lat'] . ',' . $spatial['lng']);
    $filter->condition('d', $spatial['radius']);
    $filter->condition('sort', 'geodist() asc');
    $query->filter($filter);
    
    */
  }
}

function _search_api_spatial_get_spatialfield($index_id) {

  $index = search_api_index_load($index_id);

  if (isset($index->options['processors']['spatial']) && $index->options['processors']['spatial']['status']) {
    foreach ($index->options['processors']['spatial']['settings']['fields'] as $field => $value) {
      if ($value)
        return $value;
    }
  }

  return FALSE;
}



function _search_api_spatial_get_spatial($keys) {

  $spatial = array();
  if (is_array($keys))
    $parts = explode("--spatial--", $keys[0]);
  else
    $parts = explode("--spatial--", $keys);
  
  if (isset($parts[1])) {
    $spatial_data = explode('_', $parts[1]);

    $spatial['lat'] = $spatial_data[0];
    $spatial['lng'] = $spatial_data[1];
    $spatial['radius'] = $spatial_data[2];

    return $spatial;
  }
  else {
    return FALSE;
  }

}

function _search_api_spatial_get_keys($keys) {

  if (is_array($keys))
    $parts = explode("--spatial--", $keys[0]);
  else
    $parts = explode("--spatial--", $keys);
  
  return $parts[0];

}
