<?php

/**
 * @file
 * Provides the views handler for location fields
 */

/**
 * Views filter handler base class for handling all "normal" cases.
 */
class SearchApiViewsHandlerFilterLocation extends SearchApiViewsHandlerFilter {

  /**
   * Provide a form for setting the filter value.
   */
  public function value_form(&$form, &$form_state) {

    $form['value'] = array(
      '#type' => 'search_api_spatial_pick',
      '#default_value' => array(
        'lat' => isset($this->options['lat']) ? $this->options['lat'] : '51.03826129999999',
        'lng' => isset($this->options['lng']) ? $this->options['lng'] : '3.7125661000000036',
        'radius' => isset($this->options['radius']) ? $this->options['radius'] : '5',
      ),
    );
  }
  
  public function option_definition() {
    $options = parent::option_definition();

    $options['lat'] = array('default' => '51.03826129999999');
    $options['lng'] = array('default' => '3.7125661000000036');
    $options['radius'] = array('default' => '5');

    return $options;
  }

  
  
  public function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    unset($form['operator']);
    unset($form['value']);
        
    $form['lat'] = array(
      '#title' => t('Latitude'),
      '#type' => 'textfield',
      '#default_value' => $this->options['lat'],
    );
    
    $form['lng'] = array(
      '#title' => t('Longitude'),
      '#type' => 'textfield',
      '#default_value' => $this->options['lng'],
    );
    
    $form['radius'] = array(
      '#title' => t('radius'),
      '#type' => 'textfield',
      '#default_value' => $this->options['radius'],
    );
   }
  
  /**
   * Add this filter to the query.
   */
  public function query() {
   
    
    if (isset($this->view->exposed_input['radius'])) {

      $spatial = array();
      $spatial['lat'] = $this->view->exposed_input['lat'];
      $spatial['lng'] = $this->view->exposed_input['lng'];
      $spatial['radius'] = $this->view->exposed_input['radius'];
      $spatial['field'] = $this->field;

      $this->query->setOption('spatial', $spatial);

    }
  }
}
